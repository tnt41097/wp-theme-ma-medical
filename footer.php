	<div id="fixedSection">
        <p class="fixedButton">
            <a href="#" class="hover"><img src="<?php themeUrl(); ?>/assets/images/common/fixed-button.svg" class="pc" alt=""><img src="<?php themeUrl(); ?>/assets/images/common/fixed-button-sp.svg" class="sp" alt=""></a>
        </p>
        <p class="scrollToTop">
            <a href="javascript:;" class="hover"><img src="<?php themeUrl(); ?>/assets/images/common/ico-totop.svg" alt=""></a>
        </p>
    </div>
    <!-- #fixedSection -->
    <div id="footer">
        <div class="inner">
            <div class="ftWrapper">
                <div class="ftInfo">
                    <div class="logo">
                        <a href="<?php homeUrl(); ?>"><img src="<?php themeUrl(); ?>/assets/images/common/logo.svg" alt=""></a>
                    </div>
                    <p id="copyright">© Associates Japan Co., Ltd. All Rights Reserved.</p>
                </div>
                <!-- ftInfo -->
                <div class="ftMenu">
                    <?php
                        show_my_wp_menu(
                            "footer_first_menu",
                            "menu",
                            "ftMenu",
                            true,
                            '<ul class="%2$s" id="main-menu-ul">%3$s</ul>',
                            false
                        );
                    ?>
                    <?php
                        show_my_wp_menu(
                            "footer_second_menu",
                            "menu",
                            "ftMenu",
                            true,
                            '<ul class="%2$s" id="main-menu-ul">%3$s</ul>',
                            false
                        );
                    ?>
                    <?php
                        show_my_wp_menu(
                            "footer_third_menu",
                            "menu",
                            "ftMenu",
                            true,
                            '<ul class="%2$s" id="main-menu-ul">%3$s</ul>',
                            false
                        );
                    ?>
                </div>
                <!-- ftMenu -->
            </div>
        </div>
    </div>
    <!-- #footer -->
	<?php wp_footer(); ?>
</body>

</html>