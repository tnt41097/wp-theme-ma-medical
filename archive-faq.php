<?php
    $args = [
        'post_type'         =>  'faq',
        'post_status'       =>  'publish',
        'posts_per_page'    =>  -1,
        'orderby'           =>  'title',
        'order'             =>  'ASC'
    ];

    $query = new WP_Query($args);

    get_header();

	get_template_part('/template-parts/breadcrumb-title');
?>

    <div id="content">
        <div class="areaFaq pageBG">
            <div class="inner">
                <h3 class="areaTitleLead">よくある質問</h3>
                <?php if ($query->have_posts()): ?>
                    <div class="listFaq">
                        <?php while ($query->have_posts()):
                            $query->the_post();
                        ?>
                            <?php get_template_part('template-parts/faq-item') ?>
                        <?php endwhile;
                            wp_reset_postdata();
                        ?>
                    </div>
                <?php else: ?>
                    <?php get_template_part('template-parts/no-item') ?>
                <?php endif ?>
            </div>
        </div>
    </div>

    <?php get_template_part('/template-parts/area-contact') ?>

<?php get_footer() ?>