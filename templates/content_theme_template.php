<?php
/* 
*	Template Name: Content Theme Template
*/
?>

<?php get_header(); ?>

	<?php get_template_part('/template-parts/breadcrumb-title') ?>
           
	<?php while ( have_posts() ) : the_post(); ?>
		
		<div id="content">
			<?php
				global $post;
				// CHECK PASSWORD
				if( post_password_required() ) {
					the_content();
				} else {
					$pageName = $post->post_name;
					$post_parent = ($post->post_parent > 0) ? get_post($post->post_parent) : null;
					
					if( $post_parent ){
						$pageName = $post_parent->post_name . '-' . $pageName;
					}
					get_template_part( 'pages/'.$pageName );
				}
			?>
		</div>

	<?php endwhile; ?>

	
<?php get_footer(); ?>
