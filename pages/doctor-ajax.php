<!-- File "doctor-ajax.php". Rename to "doctor.php" to run on AJAX.  -->
<div class="areaListDoctor pageBG">
    <div class="doctorIntro">
        <div class="inner">
            <h3 class="areaTitleLead">医師一覧</h3>
            <p class="txtDoc">MAメディカル相談サービス</p>
            <ul class="listDoc">
                <li>MAオンライン・セカンドオピニオンサービス（医師とオンライン面談）</li>
                <li>MA医師との相談サービス（医師とメール相談）</li>
            </ul>
            <p class="txtDoc">を受けられる医師をご紹介いたします。</p>
        </div>
    </div>
    <!-- .doctorIntro -->
    
    <div id="resultBoxArea">
        <?php get_template_part("template-parts/search-form-ajax") ?>
    </div>
    <!-- .formSearch -->

    <div class="formResult">
        <div class="inner">
            <div class="listResult" id="doctorList">
                <!-- AJAX LIST -->
                <!-- AJAX PAGINATION -->
            </div>
            <div class="boxBook">
                <h3 class="titleBook">医師の詳細なプロフィールは<br>医師への相談・面談予約<br class="sp">お申込み時に<br class="sp">ご確認いただけます。</h3>
                <p class="btnBook"><a href="#" class="hover">医師への相談・<br class="sp">面談予約をする</a></p>
            </div>
        </div>
    </div>
    <!-- .formResult -->
</div>
<!-- .areaListDoctor -->

<input type="hidden" id="pagiNumInput" name="page" disabled>