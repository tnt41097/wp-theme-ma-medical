<!-- File "doctor-php.php". Rename to "doctor.php" to run on PHP.  -->
<?php
    $category   =   isset($_GET['category']) ? $_GET['category'] : '';
    $keysearch  =   isset($_GET['keysearch']) ? $_GET['keysearch'] : '';
    $pagi_num   =   get_query_paged();
    
    $query = show_doctors($category, $keysearch, $pagi_num);
?>

<div class="areaListDoctor pageBG">
    <div class="doctorIntro">
        <div class="inner">
            <h3 class="areaTitleLead">医師一覧</h3>
            <p class="txtDoc">MAメディカル相談サービス</p>
            <ul class="listDoc">
                <li>MAオンライン・セカンドオピニオンサービス（医師とオンライン面談）</li>
                <li>MA医師との相談サービス（医師とメール相談）</li>
            </ul>
            <p class="txtDoc">を受けられる医師をご紹介いたします。</p>
        </div>
    </div>
    <!-- .doctorIntro -->
    
    <?php if (!$category && !$keysearch): ?>
        <?php get_template_part('template-parts/search-form') ?>
    <?php elseif ($query->have_posts()):
        $args = [
            'posts_count'   =>  $query->found_posts,
            'category'      =>  $category,
            'keysearch'     =>  $keysearch
        ];
    ?>
        <?php get_template_part('template-parts/result-box', null, $args) ?>
    <?php else: ?>
        <?php get_template_part('template-parts/no-result-box') ?>
    <?php endif ?>
    <!-- .formSearch -->

    <div class="formResult">
        <div class="inner">
            <?php if ($query->have_posts()): ?>
                <div class="listResult">
                    <?php while ($query->have_posts()):
                        $query->the_post();
                    ?>
                        <?php echo do_shortcode('[doctor_item_shortcode]') ?>
                    <?php endwhile;
                        wp_reset_postdata();
                    ?>
                </div>                
                <!-- PAGINATION -->
                <?php theme_pagination($query) ?>
            <?php endif ?>
            <div class="boxBook">
                <h3 class="titleBook">医師の詳細なプロフィールは<br>医師への相談・面談予約<br class="sp">お申込み時に<br class="sp">ご確認いただけます。</h3>
                <p class="btnBook"><a href="#" class="hover">医師への相談・<br class="sp">面談予約をする</a></p>
            </div>
        </div>
    </div>
    <!-- .formResult -->
</div>
<!-- .areaListDoctor -->