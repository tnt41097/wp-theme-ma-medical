<div class="itemFaq">
    <div class="question">
        <p class="numQ">
            <?php if (get_the_title()): ?>   
                <?php the_title() ?>
            <?php endif ?>
        </p>
        <h4 class="titlequestion">
            <?php the_content() ?>
        </h4>
    </div>
    <div class="anwser">
        <p class="iconA">A</p>
        <?php if (get_field('answer')): ?>
            <p class="subQuestion">
                <?php the_field('answer') ?>
            </p>
        <?php endif ?>
    </div>
</div>