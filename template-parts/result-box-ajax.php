<div class="formSearch result" id="resultBox">
    <div class="inner">
        <div class="formSearchBox">
            <h3 class="formTitle">検索結果</h3>
            <h4 class="textLg"><?php echo $args['result_count'] ?>名 見つかりました</h4>
            <ul class="areaSpecialized">
                <?php if ($args['result_category_name']): ?>
                    <li><span class="label">専門分野:　</span><br class="sp"><span class="value"><?php echo $args['result_category_name'] ?></span></li>
                <?php endif ?>
                <?php if (!empty($args['result_keysearch'])): ?>
                    <li><span class="label">キーワード検索:　</span><br class="sp"><span class="value"><?php echo $args['result_keysearch'] ?></span></li>
                <?php endif ?>
                </ul>
            <p class="btnSpecialized" id="btnBackToList"><a class="hover">違う条件で再検索する</a></p>
        </div>
    </div>
</div>