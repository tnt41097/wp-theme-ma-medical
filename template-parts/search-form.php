<?php
    $categories = get_categories();

    $category   =   isset($_GET["category"]) ? $_GET["category"] : "";
    $keysearch  =   isset($_GET["keysearch"]) ? $_GET["keysearch"] : "";

    $search_url = home_url("/doctor/?category=".$category."&keysearch=".$keysearch);
?>

<div class="formSearch">
    <div class="inner">
        <div class="formSearchBox">
            <form method="get" action="<?php echo $search_url ?>">
                <h3 class="formTitle">医師を検索する</h3>
                <div class="formContent">
                    <div class="formField">
                        <p class="formLabel">専門分野</p>
                        <div class="formInput">
                            <select name="category" class="formInputSelect">
                                <option value="">選択してください</option>
                                <?php
                                    if ($categories):
                                    foreach ($categories as $cat): 
                                ?>
                                    <option value="<?php echo $cat->slug ?>" <?php echo ($cat->slug == $category ? "selected" : "") ?>>
                                        <?php echo $cat->name ?>
                                    </option>
                                <?php
                                    endforeach;
                                    endif;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="formField">
                        <p class="formLabel">検索キーワード</p>
                        <div class="formInput">
                            <input type="text" name="keysearch" class="formInputText" placeholder="キーワードを入力してください" value="<?php echo $keysearch ?>">
                        </div>
                    </div>
                    <input type="submit" id="btnSearch" class="formInputSubmit hover" value="この条件で検索する">
                </div>
            </form>
        </div>
    </div>
</div>