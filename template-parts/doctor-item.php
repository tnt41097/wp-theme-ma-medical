<div class="resultItem">
	<div class="resultLeft">
		<?php if ($args['doctor_id']): ?>
			<p class="resultNum">
				<span class="numLabel">医師No.</span>
				<span class="numNumber"><?php echo $args['doctor_id'] ?></span>
			</p>
		<?php endif ?>
		<p class="resultAvatar">
			<a href="<?php the_permalink() ?>">
				<?php if ($args['img_url']): ?>
					<img src="<?php echo $args['img_url'] ?>" alt="">
				<?php endif ?>
			</a>
		</p>
		<p class="resultName"><?php the_field("doctor_type") ?></p>
	</div>
	<div class="resultRight">
		<?php if (has_category()): ?>
			<div class="resultField">
				<h3 class="rFTitle">専門分野</h3>
				<ul class="rFList">
					<?php theTaxsPost() ?>
				</ul>
			</div>
		<?php endif ?>
		<?php if (get_field("qualifications_awards")): ?>
			<div class="resultField">
				<h3 class="rFTitle">資格・受賞歴</h3>
				<p class="rFText"><?php the_field("qualifications_awards") ?></p>
			</div>
		<?php endif ?>
		<?php if (get_field("yoe")): ?>
			<div class="resultField">
				<h3 class="rFTitle">経験年数・経歴など</h3>
				<p class="rFText"><?php the_field("yoe") ?>年</p>
			</div>
		<?php endif ?>
	</div>
</div>