<?php
    $category       =   $args['category'];
    $keysearch      =   $args['keysearch'];
    $posts_count    =   $args['posts_count'];

    if ($category) {
        $category_obj   = get_category_by_slug($category);
        $category_name  = $category_obj->name;
    }
?>

<div class="formSearch result">
    <div class="inner">
        <div class="formSearchBox">
            <h3 class="formTitle">検索結果</h3>
            <h4 class="textLg"><?php echo $posts_count ?>名 見つかりました</h4>
            <ul class="areaSpecialized">
                <?php if ($category): ?>
                    <li><span class="label">専門分野:　</span><br class="sp"><span class="value"><?php echo $category_name ?></span></li>
                <?php endif ?>
                <?php if ($keysearch): ?>
                    <li><span class="label">キーワード検索:　</span><br class="sp"><span class="value"><?php echo $keysearch ?></span></li>
                <?php endif ?>
                </ul>
            <p class="btnSpecialized"><a href="<?php echo homeUrl()."/doctor"; ?>" class="hover">違う条件で再検索する</a></p>
        </div>
    </div>
</div>