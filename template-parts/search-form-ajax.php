<?php
    $categories = get_categories();
?>

<div class="formSearch" id="searchBox">
    <div class="inner">
        <div class="formSearchBox">
            <form id="formSearchDoctors">
                <h3 class="formTitle">医師を検索する</h3>
                <div class="formContent">
                    <div class="formField">
                        <p class="formLabel">専門分野</p>
                        <div class="formInput">
                            <select name="category" id="categorySelect" class="formInputSelect">
                                <option value="">選択してください</option>
                                <?php
                                    if ($categories):
                                    foreach ($categories as $cat): 
                                ?>
                                    <option value="<?php echo $cat->slug ?>">
                                        <?php echo $cat->name ?>
                                    </option>
                                <?php
                                    endforeach;
                                    endif;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="formField">
                        <p class="formLabel">検索キーワード</p>
                        <div class="formInput">
                            <input type="text" name="keysearch" id="keysearchInput" class="formInputText" placeholder="キーワードを入力してください" value="">
                        </div>
                    </div>
                    <input type="submit" id="btnSearch" class="formInputSubmit" value="この条件で検索する">
                </div>
            </form>
        </div>
    </div>
</div>