<div id="breadCrumb">
    <div class="inner">
        <ul class="listBread">
            <li><a href="<?php homeUrl() ?>">TOP</a></li>
            <li>
                <?php
                    if (!is_post_type_archive()) {
                        echo (get_the_title($post->post_parent) ? get_the_title($post->post_parent) : get_the_title($post));
                    }
                    show_my_archive_title();
                ?>
            </li>
        </ul>
    </div>
</div>
<!-- #breadCrumb -->
<div id="main">
    <div class="inner">
        <h2 class="pageTitle">
            <?php
                if (!is_post_type_archive()) {
                    the_title();    
                }
                show_my_archive_title();
            ?>
        </h2>
    </div>
</div>
<!-- #main -->