<?php
/**
 * THEME DEFINE
 */


// THEME URL
define( 'THEME_URL', get_stylesheet_directory_uri() );

// THEME DISK SRC
define( 'THEME_PATH', get_stylesheet_directory() );

// HOME URL
define( 'HOME_URL', get_home_url() );

// THEME NAME
define( 'THEME_NAME', 'mytheme' );

// NO IMAGE ID
define( 'NOIMAGE_ID', 0 );

// ON/OFF CONTENT THEME SYNC EDITER
define( 'HAS_CTTHEME_EDITER', false );

// REGISTER NEW IMAGES SIZE
define('DOCTOR_IMAGE_SIZE', [
	'doctor-thumbnail' => [
		'width' 	=> 	400,
		'height'	=>	400,
		'crop' 		=> 	false
	]
]);

define('POST_TYPE_SLUG_POST', 'post');

define('BASE_DOCTOR_IMG_URL', THEME_URL.'/assets/images/doctor');