<!DOCTYPE html>
<html>

<head>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="header">
        <div class="headerBar">
            <div class="inner">
                <div class="logo">
                    <a class="hover" href="<?php homeUrl(); ?>"><img src="<?php themeUrl(); ?>/assets/images/common/logo.svg" alt=""></a>
                </div>
                <?php
                    show_my_wp_menu(
                        "header_language_menu",
                        "listLang",
                        "inner",
                        true,
                        '<ul class="%2$s" id="main-menu-ul">%3$s</ul>',
                        false
                    );
                ?>
                <div class="hamburger sp">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="mainMenu">
            <div class="inner">
                <?php
                    show_my_wp_menu(
                        "header_main_menu",
                        "menu",
                        "inner",
                        true,
                        '<ul class="%2$s" id="main-menu-ul">%3$s</ul>',
                        false
                    );
                ?>
            </div>
        </div>
    </div>
    <!-- #header -->
    <div id="fixH"></div>