<?php

use AC\Column\Comment\Excerpt;

/**
 * MAIN FUNCTIONS
 */

// 1. Create post_meta doctor_id and update it when existed (required $post_id as parameter)
add_action('save_post', 'create_my_post_meta_doctor_id', 10, 1);
function create_my_post_meta_doctor_id($post_id) {
    if (get_post_type($post_id) == POST_TYPE_SLUG_POST) {
        $doctor_id = get_post_meta($post_id, 'doctor_id', true);
        if (!$doctor_id) {
            update_post_meta($post_id, 'doctor_id', $post_id);
        }
    }
}

// 2. Update post_name (slug) of post when save post (required $post_id as parameter)
add_action('save_post', 'update_my_post_name', 10, 1);
function update_my_post_name($post_id) {
	if (!wp_is_post_revision($post_id) && get_post_type($post_id) == POST_TYPE_SLUG_POST) {
		remove_action('save_post', 'update_my_post_name');
        $doctor_id = get_post_meta($post_id, 'doctor_id', true) ? get_post_meta($post_id, 'doctor_id', true) : '';

		wp_update_post([
            'ID'        =>  $post_id,
            'post_name' =>  'doctor-'.$doctor_id
        ]);

		add_action('save_post', 'update_my_post_name');
	}
}

// 3. Rewrite permalink of post
add_filter('post_link', 'rewrite_my_post_permalink', 10, 2);
function rewrite_my_post_permalink($permalink, $post) {
    $slug = $post->post_name;
    $custom_permalink = str_replace($slug, 'doctor/'.$slug, $permalink);
    return $custom_permalink;
}

// 4. add_rewrite_rule of post single
add_action('init', 'my_rewrite_rule');
function my_rewrite_rule() {
    add_rewrite_rule('^doctor/([^/]+)/?$', 'index.php?post_type=post&name=$matches[1]', 'top');
}



//-----------------------------------------------------------------------------------------------------------------------

// THE TAXS IN A POST
function theTaxsPost( $tax_slug = 'category' ) {
    global $post;
    if( !$post ) return false;
    $tax_slug = ($tax_slug) ? $tax_slug : 'category';
    // GET RESULT
    $taxs_html = '';
    $taxonomies = wp_get_post_terms( $post->ID, $tax_slug );
    foreach($taxonomies as $taxonomy) {
        if($taxonomy->term_id != 1) {
            $taxs_html .= '<li>'.$taxonomy->name.'</li>';
        }
    }
    echo $taxs_html;
}


// THE LIST TAX
function theTaxs( $tax_slug = 'category' ) {
    $tax_slug = ($tax_slug) ? $tax_slug : 'category';
    // GET RESULT
    $taxs_html = '';
    $taxonomies = get_terms( $tax_slug );
    foreach($taxonomies as $taxonomy) {
        if($taxonomy->term_id != 1) {
            $taxs_html .= '<li><a href="'. get_term_link($taxonomy) .'">'.$taxonomy->name.'</a></li>';
        }
    }
    echo $taxs_html;
}


// THEME PAGINATION FUNCTION
function theme_pagination( $post_query = null ) {
    global $paged, $wp_query;
    
    $translate['next']  = '次に';
    $translate['prev']  = '前の';
    $translate['last']  = '最後';
    $translate['first'] = '初め';

    if( empty( $paged ) ) $paged = 1;
    $prev = $paged - 1;             
    $next = $paged + 1;
    
    $end_size = 1;
    $mid_size = 2;
    $show_all = false;
    $dots = false;

    $pagi_query = $wp_query;
    if( isset($post_query) && $post_query ) {
        $pagi_query = $post_query;
    }

    if( ! $total = $pagi_query->max_num_pages ) $total = 1;
    
    if( $total > 1 )
    {
        echo '<div class="pagingNav">';
        echo '<ul class="pagi_nav_list">';
        
        if( $paged > 1 ){
            echo '<li class="p-control prev"><a href="'. get_pagenum_link(1) .'">'. $translate['first'] .'</a></li>';
            echo '<li class="p-control prev"><a href="'. previous_posts(0, false) .'">'. $translate['prev'] .'</a></li>';
        }

        for( $i = 1; $i <= $total; $i++ ){
            if ( $i == $paged ){
                echo '<li class="active"><a>'. $i .'</a></li>';
                $dots = true;
            } else {
                if ( $show_all || ( $i <= $end_size || ( $paged && $i >= $paged - $mid_size && $i <= $paged + $mid_size ) || $i > $total - $end_size ) ){
                    echo '<li><a href="'. get_pagenum_link($i) .'">'. $i .'</a></li>';
                    $dots = true;
                } elseif ( $dots && ! $show_all ) {
                    echo '<li class="dots"><a>...</a></li>';
                    $dots = false;
                }
            }
        }

        if( $paged < $total ){
            echo '<li class="p-control next"><a href="'. next_posts(0, false) .'">'. $translate['next'] .'</a></li>';
            echo '<li class="p-control next"><a href="'. get_pagenum_link($total) .'">'. $translate['last'] .'</a></li>';
        }

      	echo '</ul>';
      	echo '</div>';
    }
}


// GET QUERY PAGED NUMBER
function get_query_paged() {
	return (get_query_var('paged')) ? get_query_var('paged') : 1;
}


// THE SINGLE PAGINATION
function the_single_pagination() {
    $prev_post = get_previous_post();
    $next_post = get_next_post();
?>
    <ul class="pagination">
        <?php if( $prev_post ): ?>
            <li><a href="<?php echo get_permalink( $prev_post->ID ); ?>" class="prev">ウィンターセール開催 !</a></li>
        <?php endif; ?>
        <?php if( $next_post ): ?>
            <li><a href="<?php echo get_permalink( $next_post->ID ); ?>" class="next">マリアナ　イベント開催 !</a></li>
        <?php endif; ?>
    </ul>
<?php
}


function my_theme_support() {
	register_nav_menus(
        [
            "header_language_menu"	=>	"Header Language Menu"
		]
    );
	register_nav_menus(
        [
            "header_main_menu"	=>	"Header Main Menu"
		]
    );
	register_nav_menus(
        [
            "footer_first_menu"	=>	"Footer First Menu"
		]
    );
	register_nav_menus(
        [
            "footer_second_menu"	=>	"Footer Second Menu"
		]
    );
	register_nav_menus(
        [
            "footer_third_menu"	=>	"Footer Third Menu"
		]
    );
}

add_action("after_setup_theme", "my_theme_support");

// CREATE WP MENU
function show_my_wp_menu($theme_location, $menu_class, $container_class, $is_container, $items_wrap, $is_fallback_cb) {
    wp_nav_menu(
        [
            "theme_location"    =>  $theme_location,
            "menu_class"        =>  $menu_class,
            "container_class"   =>  $container_class,
            "container"         =>  $is_container,
            "items_wrap"        =>  $items_wrap,
            "fallback_cb"       =>  $is_fallback_cb
        ]
    );
}

function show_my_sub_field($sub_field_name) {
    if (get_sub_field($sub_field_name)) {
        the_sub_field($sub_field_name);
    }
}


function show_doctors($category ='', $keysearch ='', $pagi_num = 1) {
    $common_args = [
        'post_type'         => POST_TYPE_SLUG_POST,
        'post_status'       => 'publish',
        'posts_per_page'    => 5,
        'paged'             => $pagi_num
    ];

    $category_args = [
        'tax_query' => [
            [
                'taxonomy'  =>  'category',
                'field'     =>  'slug',
                'terms'     =>  $category
            ]
        ]
    ];

    $keysearch_args = [
        'meta_query'        =>  [
            'relation'      =>  'OR',
            [
                'key'       =>  'doctor_id',
                'value'     =>  $keysearch,
                'compare'   =>  '='
            ],
            [
                'key'       =>  'introduce',
                'value'     =>  $keysearch,
                'compare'   =>  'LIKE'
            ],
            [
                'key'       =>  'organization_position_organization',
                'value'     =>  $keysearch,
                'compare'   =>  'LIKE'
            ],
            [
                'key'       =>  'organization_position_position',
                'value'     =>  $keysearch,
                'compare'   =>  'LIKE'
            ],
            [
                'key'       =>  'qualifications_awards',
                'value'     =>  $keysearch,
                'compare'   =>  'LIKE'
            ]
        ]
    ];

    if (empty($category) && empty($keysearch)) {
        $args = $common_args;
    } else if (!empty($category) && empty($keysearch)) {
        $args = array_merge($common_args, $category_args);
    } else if (empty($category) && !empty($keysearch)) {
        $args = array_merge($common_args, $keysearch_args);
    } else {
        $args = array_merge($common_args, $category_args, $keysearch_args);
    }

    $query = new WP_Query($args);
    return $query;
}


function show_my_archive_title() {
    if (is_post_type_archive()) {
        $title          = get_the_archive_title();
        $final_title    = str_replace('Archives:', '', $title);
        echo $final_title;
    }
}


// AJAX DOCTORS (need adjust)
function theme_pagination_by_ajax($post_query = null) {
    global $wp_query;
    $page = get_query_var("pagi_num");
    
    $translate['next']  = '次に';
    $translate['prev']  = '前の';
    $translate['last']  = '最後';
    $translate['first'] = '初め';

    if ( empty($page) ) $page = 1;
    $prev = $page - 1;
    $next = $page + 1;
    
    $end_size = 1;
    $mid_size = 2;
    $show_all = false;
    $dots = false;

    $pagi_query = $wp_query;
    if ( isset($post_query) && $post_query ) {
        $pagi_query = $post_query;
    }

    if ( !$total = $pagi_query->max_num_pages ) $total = 1;
    
    if ( $total > 1 ) {
        echo '<div class="pagingNav">';
        echo '<ul class="pagi_nav_list">';
        
        if ( $page > 1 ) {
            echo '<li class="p-control prev pagiButton" data-page="1"><a>'. $translate['first'] .'</a></li>';
            echo '<li class="p-control prev pagiButton" data-page="'. $prev .'"><a>'. $translate['prev'] .'</a></li>';
        }

        for ( $i = 1; $i <= $total; $i++ ) {
            if ( $i == $page ) {
                echo '<li class="active pagiButton" data-page="'. $i .'"><a>'. $i .'</a></li>';
                $dots = true;
            } else {
                if ( $show_all || ( $i <= $end_size || ( $page && $i >= $page - $mid_size && $i <= $page + $mid_size ) || $i > $total - $end_size ) ) {
                    echo '<li class="pagiButton" data-page="'. $i .'"><a>'. $i .'</a></li>';
                    $dots = true;
                } elseif ( $dots && !$show_all ) {
                    echo '<li class="dots"><a>...</a></li>';
                    $dots = false;
                }
            }
        }

        if ( $page < $total ) {
            echo '<li class="p-control next pagiButton" data-page="'. $next .'"><a>'. $translate['next'] .'</a></li>';
            echo '<li class="p-control next pagiButton" data-page="'. $total .'"><a>'. $translate['last'] .'</a></li>';
        }

      	echo '</ul>';
      	echo '</div>';
    }
}


function show_doctors_and_pagination_html_by_ajax($category = '', $keysearch = '', $pagi_num = 1) {
    $category   =   $_POST["category"];
    $keysearch  =   $_POST["keysearch"];
    $pagi_num   =   $_POST["page"];

    set_query_var("pagi_num", $pagi_num);

    $query = show_doctors($category, $keysearch, $pagi_num);

    // AJAX LIST
    $doctor_html = '';
    ob_start();
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $doctor_html .= do_shortcode('[doctor_item_shortcode]');
            }
            wp_reset_postdata();
        }
    ob_end_clean();

    // AJAX PAGINATION
    ob_start();
        theme_pagination_by_ajax($query);
    $pagination_html = ob_get_clean();

    $response = [
        'doctor_html'       =>  $doctor_html,
        'pagination_html'   =>  $pagination_html
    ];

    wp_send_json($response);
}

add_action('wp_ajax_show_doctors_and_pagination_html_by_ajax', 'show_doctors_and_pagination_html_by_ajax');
add_action('wp_ajax_nopriv_show_doctors_and_pagination_html_by_ajax', 'show_doctors_and_pagination_html_by_ajax');


function show_result_box_html_by_ajax($category_slug = '', $keysearch = '', $pagi_num = 1) {
    $category_slug = $_POST['category'];
    $keysearch = $_POST['keysearch'];
    $pagi_num = get_query_var('pagi_num');

    $query = show_doctors($category_slug, $keysearch, $pagi_num);

    ob_start();
    if ($query->have_posts()) {
        $category_obj = get_category_by_slug($category_slug);

        if (($category_obj)) {
            $category_name = $category_obj->name;
        }

        $count = $query->found_posts;

        $args = [
            'result_keysearch'      =>  $keysearch,
            'result_category_name'  =>  isset($category_name) ? $category_name : null,
            'result_count'          =>  $count
        ];

        get_template_part('template-parts/result-box-ajax', null, $args);
    } else {
        get_template_part('template-parts/no-result-box');
    }

    $html = ob_get_contents();
    ob_clean();

    return wp_send_json($html);
}

add_action('wp_ajax_show_result_box_html_by_ajax', 'show_result_box_html_by_ajax');
add_action('wp_ajax_nopriv_show_result_box_html_by_ajax', 'show_result_box_html_by_ajax');