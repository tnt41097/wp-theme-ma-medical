<?php

function add_doctor_item_shortcode($atts) {
    $shortcode_args = shortcode_atts([
        'doctor_id' =>  get_field("doctor_id"),
        'img_url'   =>  get_field('gender') == 'male' ? BASE_DOCTOR_IMG_URL.'/ava-men.jpg' : BASE_DOCTOR_IMG_URL.'/ava-women.jpg'
    ], $atts);

    $args = [
        'doctor_id' =>  $shortcode_args['doctor_id'],
        'img_url'   =>  $shortcode_args['img_url']
    ];

    ob_start();
        get_template_part('template-parts/doctor-item', null, $args);
    $html = ob_get_contents();
    ob_clean();

    return $html;
}

add_shortcode('doctor_item_shortcode', 'add_doctor_item_shortcode');


