<?php
/**
 *  SETUP ASSETS
 */


// ADD ASSETS HEAD
add_action('wp_head', 'add_theme_assets_for_head', 50);
function add_theme_assets_for_head() {
?>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="icon" type="image/png" href="favicon.png" />
    <meta name="description" content=" content " />
    <meta name="keywords" content=" content " />
    <meta name="author" content=" content " />
    <meta name="robots" content=" all " />
    <meta name="googlebot" content=" all ">

    <?php if (!is_front_page() || !is_home() || !is_page('company')): ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.theme.min.css" integrity="sha512-9h7XRlUeUwcHUf9bNiWSTO9ovOWFELxTlViP801e5BbwNJ5ir9ua6L20tEroWZdm+HFBAWBLx2qH4l4QHHlRyg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <?php endif ?>

    <link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/common.css">

    <?php if (is_front_page() || is_home()): ?>
        <link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/index.css">
    <?php elseif(is_page('doctor')): ?>
        <link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/doctor-list.css">
    <?php elseif (is_single()): ?>
        <link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/doctor-detail.css">
    <?php elseif (is_page('terms') || is_page('privacy')): ?>
        <link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/privacy.css">
    <?php elseif (is_post_type_archive('faq')): ?>
        <link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/faq.css">
    <?php elseif (is_page()):
        global $post;
        $post_slug = $post->post_name;
        $post_parent = $post->post_parent > 0 ? get_post($post->post_parent) : null;
        if ($post_parent) {
            $post_slug = $post_parent->post_name.'-'.$post_slug;
        }    
        $page_css_uri = THEME_PATH.'/assets/css/'.$post_slug.'.css';

        if (is_file($page_css_uri)):
    ?>
        <link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/<?php echo $post_slug; ?>.css">
    <?php endif; endif; ?>


    <script type="text/javascript" src="<?php themeUrl(); ?>/assets/js/jquery-1.11.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.js"></script>

    <?php if (is_page('contact')): ?>
        <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
        <script src="<?php themeUrl(); ?>/assets/libs/datepicker-ja.js"></script>
    <?php elseif (!is_front_page() || ! is_home() || ! is_page("company")): ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.js" integrity="sha512-RCgrAvvoLpP7KVgTkTctrUdv7C6t7Un3p1iaoPr1++3pybCyCsCZZN7QEHMZTcJTmcJ7jzexTO+eFpHk4OCFAg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/i18n/datepicker.ja-JP.min.js" integrity="sha512-ZP3x/vrH154LojT7mCIBPQoioAD64+Qx8LQ1LZSP5DO6gFOx79U2AMl4t3dfwKHPNRIR4MmG4/SOcgagUngtaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <?php endif ?>

    <script type="text/javascript" src="<?php themeUrl(); ?>/assets/js/script.js"></script>
<?php
}


// ADD ASSETS FOOTER
add_action( 'wp_footer', 'add_theme_assets_for_footer', 50 );
function add_theme_assets_for_footer() {
    ?>
        <?php if (is_page('contact')): ?>
            <script type="text/javascript" src="<?php themeUrl(); ?>/assets/js/contact.js"></script>
        <?php elseif (is_post_type_archive('faq')): ?>
            <script type="text/javascript" src="<?php themeUrl(); ?>/assets/js/faq.js"></script>
        <?php endif ?>
    <?php
}

add_action( 'wp_footer', 'add_doctor_script', 50 );
function add_doctor_script() {
    if (is_page('doctor')) {
        ?>
        <script>
            const admin_ajax_url = '<?php echo admin_url("admin-ajax.php") ?>';

            const disableBtnSearch = () => {
                $("#btnSearch").attr("disabled", "disabled");
            }

            const enableBtnSearch = () => {
                $("#btnSearch").removeAttr("disabled");
            }


            const showDoctorsAndPagination = (categorySlug, keysearchValue, pagiNum) => {
                disableBtnSearch();
                $("#doctorList").html("");

                console.log("category: " + categorySlug);
                console.log("keysearch: " + keysearchValue);
                console.log("page: " + pagiNum);

                $.ajax({
                    url: admin_ajax_url,
                    method: "POST",
                    data: {
                        action: "show_doctors_and_pagination_html_by_ajax",
                        category: categorySlug,
                        keysearch: keysearchValue,
                        page: pagiNum
                    },
                    success: resp => {
                        $("#doctorList").append(resp.doctor_html, resp.pagination_html);
                        enableBtnSearch();
                    },
                    error: () => {
                        console.error("Error(s) on showing list");
                        enableBtnSearch();
                    }
                });
            }


            const submitSearchDoctorsEvent = () => {
                $("#formSearchDoctors").on("submit", e => {
                    e.preventDefault();

                    let categorySlug    = $("#categorySelect").val();
                    let keysearchValue  = $("#keysearchInput").val();

                    if (categorySlug || keysearchValue) {
                        showResultBox(categorySlug, keysearchValue);
                        showDoctorsAndPagination(categorySlug, keysearchValue, 1);
                        paginationEvent();
                    } else if ($("#pagiNumInput").val() != 1) {
                        $("#pagiNumInput").val(1);
                        showDoctorsAndPagination("", "", 1);
                    }
                });
            }


            const showResultBox = (categorySlug, keysearchValue) => {
                $.ajax({
                    url: admin_ajax_url,
                    method: "POST",
                    data: {
                        action: "show_result_box_html_by_ajax",
                        category: categorySlug,
                        keysearch: keysearchValue
                    },
                    success: resp => {
                        $("#searchBox").addClass("hide");
                        $("#resultBoxArea").append(resp);
                    },
                    error: () => {
                        console.error("Error(s) on showing result box");
                    }
                });

                backToListEvent();
            }


            const backToListEvent = () => {
                $(document).on("click", "#btnBackToList", () => {
                    $("#resultBox").remove();
                    $("#searchBox").removeClass("hide");

                    $("#categorySelect").val("");
                    $("#keysearchInput").val("");
                    $("#pagiNumInput").val(1);
                    
                    showDoctorsAndPagination("", "", 1);
                });
            }


            const paginationEvent = () => {
                $(document).on("click", ".pagiButton", e => {
                    let pagiNum = $(e.currentTarget).data("page");

                    if (pagiNum != $("#pagiNumInput").val()) {
                        let categorySlug    = $("#categorySelect").val();
                        let keysearchValue  = $("#keysearchInput").val();

                        $("#pagiNumInput").val(pagiNum);
                        showDoctorsAndPagination(categorySlug, keysearchValue, pagiNum);
                    }
                });
            }


            jQuery(document).ready($ => {    
                $("#pagiNumInput").val(1);

                showDoctorsAndPagination("", "", 1);
                submitSearchDoctorsEvent();
                paginationEvent();
            });
        </script>
        <?php
    } 
}

