<?php get_header() ?>

    <div id="breadCrumb">
        <div class="inner">
            <ul class="listBread">
                <li><a href="<?php homeUrl() ?>">TOP</a></li>
                <li>医師一覧</li>
                <li><?php the_title() ?></li>
            </ul>
        </div>
    </div>
    <!-- #breadCrumb -->
    <div id="main">
        <div class="inner">
            <h2 class="mainTitle acumin">Specialist Doctors profile</h2>
        </div>
    </div>
    <!-- #main -->
    
    <?php while (have_posts()) : the_post() ?>
        <div id="content">
            <div class="areaDetail">
                <div class="inner">
                    <div class="boxDetail">
                        <?php if (get_field("doctor_id")): ?>
                            <p class="resultNum">
                                <span class="numLabel">医師No.</span>
                                <span class="numNumber"><?php the_field("doctor_id") ?></span>
                            </p>
                        <?php endif ?>
                        <h3 class="titleDetail"><?php the_title() ?><span class="small">医師</span></h3>
                        <div class="boxInfor">
                            <p class="imageInfor">
                                <?php
                                    if (has_post_thumbnail()) {
                                        the_post_thumbnail('doctor-thumbnail');
                                    }
                                ?>
                            </p>
                            <div class="detailInfor">
                                <?php if (get_field("organization_position")): ?>
                                    <div class="itemInfor">
                                        <h4 class="titleInfor">所属医療機関・役職</h4>
                                        <p class="subInfor sub21">
                                            <?php
                                                $organization_positions = [
                                                    "organization_position_organization", 
                                                    "organization_position_position"
                                                ];

                                                foreach ($organization_positions as $organization_position):
                                                    $organization_position_value = get_field($organization_position);
                                                    if ($organization_position_value):
                                            ?>
                                                <?php echo $organization_position_value ?><br>
                                            <?php
                                                endif;
                                                endforeach;
                                            ?>
                                        </p>
                                    </div>
                                <?php endif ?>
                                <?php if (get_field("location")): ?>
                                    <div class="itemInfor">
                                        <h4 class="titleInfor">所在地</h4>
                                        <p class="subInfor"><?php the_field("location") ?></p>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="listInfor">
                            <?php if (has_category()): ?>
                                <div class="itemInfor">
                                    <h4 class="titleInfor">専門分野</h4>
                                    <ul class="rFList">
                                        <?php theTaxsPost() ?>
                                    </ul>
                                </div>
                            <?php endif ?>
                            <?php if (get_field("qualifications_awards")): ?>
                                <div class="itemInfor">
                                    <h4 class="titleInfor">資格・受賞歴</h4>
                                    <p class="subInfor sub16"><?php the_field("qualifications_awards") ?></p>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="boxContent">
                        <?php if (get_field("introduce")): ?>
                            <div class="itemContent">
                                <h4 class="titleContent">自己紹介</h4>
                                <p class="subContent"><?php the_field("introduce") ?></p>
                            </div>
                        <?php endif ?>
                        <?php if (has_category()): ?>
                            <div class="itemContent">
                                <h4 class="titleContent">専門分野</h4>
                                <ul class="rFList">
                                    <?php theTaxsPost() ?>
                                </ul>
                            </div>
                        <?php endif ?>
                        <?php if (get_field("diseases")): ?>
                            <div class="itemContent">
                                <h4 class="titleContent">対応可能な疾患</h4>
                                <?php the_field("diseases") ?>
                            </div>
                        <?php endif ?>
                        <?php if (get_field("qualifications_awards")): ?>
                            <div class="itemContent">
                                <h4 class="titleContent">資格・受賞歴</h4>
                                <p class="subContent"><?php the_field("qualifications_awards") ?></p>
                            </div>
                        <?php endif ?>
                        <?php if (have_rows("associations")): ?>
                            <div class="itemContent">
                                <h4 class="titleContent">所属学会</h4>
                                <div class="tableContent">
                                    <table>
                                        <?php
                                            while (have_rows("associations")):
                                            the_row();
                                        ?>
                                            <tr>
                                                <th>
                                                    <?php if (get_sub_field("association_start_time")): ?>
                                                        <?php echo date("Y年m月", strtotime(get_sub_field("association_start_time"))) ?>
                                                    <?php endif ?>
                                                    <?php if (get_sub_field("association_end_time")): ?>
                                                        ～ <?php echo date("Y年m月", strtotime(get_sub_field("association_end_time"))) ?>
                                                    <?php endif ?>
                                                </th>
                                                <td>
                                                    <?php show_my_sub_field("association_name") ?>
                                                </td>
                                            </tr>
                                        <?php endwhile ?>
                                    </table>
                                </div>
                            </div>
                        <?php endif ?>
                        <?php if (get_field("yoe")): ?>
                            <div class="itemContent">
                                <h4 class="titleContent">経験年数</h4>
                                <p class="subContent"><?php the_field("yoe") ?>年</p>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile ?>
    <!-- #content -->
    <script type="text/javascript">
        $('.question').click(function(){
            $(this).next('.anwser').stop().slideToggle();
            $(this).toggleClass('changeArrs');
        });
    </script>
    <?php get_template_part("/template-parts/area-contact") ?>
    <!-- #areaContact -->

<?php get_footer() ?>